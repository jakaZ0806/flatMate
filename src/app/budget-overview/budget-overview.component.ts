import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Apollo, ApolloQueryObservable } from 'apollo-angular';
import { ApolloQueryResult } from 'apollo-client';
import * as graphQLFunctions from './budget-overview.component.queries';
import {DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { Router } from '@angular/router';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-budget-overview',
  templateUrl: './budget-overview.component.html',
  styleUrls: ['./budget-overview.component.css'],
  providers: [DecimalPipe],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', visibility: 'hidden'})),
      state('expanded', style({height: '*', visibility: 'visible'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class BudgetOverviewComponent implements OnInit {

  displayedColumns = ['name', 'budget'];
  currentUsername;
  getBudgetsQuery;
  groupBudgets = [];
  userBudgetDataSource;
  expandedElement;

  constructor(private apollo: Apollo, private cd: ChangeDetectorRef, private router: Router) { }

  ngOnInit() {
    this.currentUsername = JSON.parse(localStorage.getItem('currentUser')).username;
    this.getBudgets();
  }

  public getBudgets() {
    this.getBudgetsQuery = this.apollo.watchQuery<any>({
      query: graphQLFunctions.queries.getUserSummary,
      variables: {
        username: this.currentUsername
      }
    });
    this.getBudgetsQuery.subscribe(({data}) => {
      this.groupBudgets = data.getUserSummary.groupBudgets;
      this.userBudgetDataSource = new GroupDataSource(this.groupBudgets);
      this.cd.markForCheck();
    });
    }

    changeExpandedElement(row) {
      if (this.expandedElement === row) {
        this.expandedElement = null;
      } else {
        this.expandedElement = row;
      }
    }

    goToGroup(groupname) {
      this.router.navigate(['/budget/' + groupname]);
    }


}

class GroupDataSource extends DataSource<any> {
  constructor(private data: any[]) {
    super();
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<any[]> {
    //const rows = [];
    //this.data.forEach(element => rows.push(element, {detailRow: true, element}));
    return Observable.of(this.data);
  }

  disconnect() {}
}
