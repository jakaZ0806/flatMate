import gql from "graphql-tag";

/*---------------QUERIES--------------------*/
const getUserByUsername:any = gql`
query getUser($username: String) {
  user(username: $username) {
    username
    firstName
    lastName,
    groups {
      name
      description
      members {
        firstName
        lastName
      }
    }
  }
}
`;

const getAllBudgetsForUser: any = gql`
query getAllBudgets($username: String!) {
  getAllBudgets(username: $username) {
      group {
        name
        description
      }
      budget
  }
}
`;

const getUserSummary: any = gql`
query getUserSummary($username: String!) {
  getUserSummary(username: $username) {
      user {
        username
        firstName
        lastName
      }
      groupBudgets {
        group {
          name
          description
        }
        budgets {
          budget
          user {
            firstName
          }
        }
        ownBudget {
          budget
        }
      }
  }
}
`;

const getBudgetForGroup: any = gql`query getGroupBudget($group: String!) {
  groupBudget(group: $group) {
    groupname
    budgets {
      user {
        username
        firstName
        lastName
      }
      budget
    }
  }
}
`;



const queries = {getUserByUsername, getAllBudgetsForUser, getBudgetForGroup, getUserSummary};

const mutations = {};



export {queries, mutations};
