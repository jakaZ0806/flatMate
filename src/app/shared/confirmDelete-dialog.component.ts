import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-confirmdelete-dialog',
  template: `
    <h1 matDialogTitle>Löschen</h1>
    <mat-dialog-content>
      Wirklich löschen?
    </mat-dialog-content>
    <mat-dialog-actions>
      <button mat-raised-button (click)="closeDialogSuccess()">
        Löschen
      </button>
        <button mat-raised-button (click)="closeDialogAbort()">
        Abbrechen
      </button>
    </mat-dialog-actions>
  `,
  styles: []
})
export class ConfirmdeleteDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ConfirmdeleteDialogComponent>) { }

  closeDialogAbort() {
    this.dialogRef.close('false');
  }

  closeDialogSuccess() {
    this.dialogRef.close(true);
  }

  ngOnInit() {
  }

}
