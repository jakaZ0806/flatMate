/**
 * Created by Lukas on 14-Nov-16.
 */
import { Routes, RouterModule } from '@angular/router/';

import { BudgetComponent } from '../budget/budget.component';
import { LoginComponent } from "../login/login.component";
import { AdminpanelComponent } from '../adminpanel/adminpanel.component'
import { AuthGuard } from "../auth-guard";
import { ProfileComponent } from '../profile/profile.component';
import { BudgetOverviewComponent } from '../budget-overview/budget-overview.component';
import { ManagementComponent} from '../management/management.component';
import { GroupManagementComponent } from '../management/group-management/group-management.component';


export const routes: Routes = [
  {path: 'budget', component: BudgetOverviewComponent, canActivate: [AuthGuard]},
  { path: 'budget/:group', component: BudgetComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent},
  { path: 'profile', component: ProfileComponent, canActivate:[AuthGuard]},
  { path: 'adminpanel', component: AdminpanelComponent, canActivate: [AuthGuard]},
  { path: 'management', component: ManagementComponent, canActivate: [AuthGuard]},
  { path: 'management/:group', component: GroupManagementComponent, canActivate: [AuthGuard] },
  { path: '', redirectTo: '/budget', pathMatch: 'full'},
  {
    path: 'crisis-center',
    component: BudgetComponent,
    children: [
      {
        path: '',
        component: BudgetComponent,
        children: [
          {
            path: '',
            component: BudgetComponent
          }
        ]
      }
    ]
  },
  {path: '**', redirectTo: ''  }
];

export const routing = RouterModule.forRoot(routes);
