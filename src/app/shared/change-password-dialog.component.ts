import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';

@Component({
  selector: 'app-changepassword-dialog',
  template: `
    <h1 matDialogTitle>Change Password</h1>
    <mat-dialog-content>
    <form name="form" (ngSubmit)="formCtrl.form.valid && closeDialogSuccess(currentPassword.value, password.value)" #formCtrl="ngForm">
    <div class="form-group">
    <mat-form-field>
        <input matInput type="password" placeholder="current Password" name="currentPassword" #currentPassword required>
      </mat-form-field><br>
      </div>
      <div class="form-group">
      <mat-form-field>
        <input matInput type="password" placeholder="new Password" name="password" #password required (keyup)="comparePasswords(password.value, confirmPassword.value)">
      </mat-form-field>
      </div>
      <div class="form-group">
       <mat-form-field>
        <input matInput type="password" placeholder="confirm Password" name="confirmPassword"  #confirmPassword required (keyup)="comparePasswords(password.value, confirmPassword.value)">
      </mat-form-field>
      <br>
      <small [hidden]="passwordsEqual">
      Passwords don't match!
    </small><br>
    </div>
    <div class="form-group">

    <button mat-button class="btn btn-primary" [disabled]="!passwordsEqual || !formCtrl.form.valid">Submit</button>

    </div>
    </form>
    <button mat-button class="btn btn-primary" *ngIf="!data.mandatory" (click)="closeDialogAbort()">Abort</button>

  `,
  styles: []
})
export class ChangePasswordDialogComponent implements OnInit {
  password = '';
  confirmPassword = '';
  currentPassword = '';
  passwordsEqual = true;

  constructor(public dialogRef: MatDialogRef<ChangePasswordDialogComponent>, public cd: ChangeDetectorRef, @Inject(MAT_DIALOG_DATA) public data: any) { }

  closeDialogSuccess(currentPassword, password) {
    this.dialogRef.close({currentPassword: currentPassword, newPassword: password});
  }
  closeDialogAbort() {
    this.dialogRef.close();
  }

  comparePasswords(password, confirmPassword) {
    if (password !== confirmPassword) {
      this.passwordsEqual = false;
    } else if (password === confirmPassword) {
      this.passwordsEqual = true;
    }
    this.cd.detectChanges();
  }

  ngOnInit() {
  }

}
