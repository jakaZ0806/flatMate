import { Injectable} from '@angular/core';
import { Apollo } from 'apollo-angular';
import { ApolloQueryResult } from 'apollo-client';
import gql from 'graphql-tag';
import 'rxjs/add/operator/toPromise';
import {UserService} from './user.service';
@Injectable()
export class AuthenticationService {
  public token: string;

  constructor(private apollo: Apollo, private userService: UserService) {
    // set token if saved in local storage
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;

  }

  private getToken: any = gql`
        query login($username: String!, $password: String!) {
          login(username: $username, password: $password) {
           	success
           	error
             token
            userActions {
              changePassword
            }
           	}
        }
      `;


  login(username, password): Promise<string> {
    return this.apollo.query({
      //get Token from Server for user-password combination
      query: this.getToken,
      variables: {
        username: username,
        password: password
      }
    })
      .toPromise()
      .then(({data}: ApolloQueryResult<any>) => {
        if (data.login.success) {
          //Store Token in local Storage
          localStorage.setItem('currentUser', JSON.stringify({ username: username, token: data.login.token }));
          this.userService.updateCurrentUserData();
          if (data.login.userActions.changePassword) {
            return 'changePassword';
          } else {
          return 'success';
          }
        } else {
          return data.login.error;
        }
      });

  };

  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    localStorage.removeItem('currentUser');
    this.userService.logoutUser();

  }

}
