import {Injectable, OnInit, EventEmitter} from '@angular/core';
import { Apollo } from 'apollo-angular';
import { ApolloQueryResult } from 'apollo-client';
import { AuthenticationService } from './authentication.service';
import gql from 'graphql-tag';
import {Observable} from 'rxjs/Observable';

interface UserQueryResponse {
  getSelf
  loading
}

@Injectable()
export class UserService {
  constructor(private apollo: Apollo) {
  }

  loading;
  public currentUser: any = {firstName: 'Gast', admin: 'false'};

  userUpdated: EventEmitter<any> = new EventEmitter();


  private getSelf:any = gql`
        query {
          getSelf {
            firstName
            lastName
            username
            superUser
            admin
            groups {
              name
            }
          }
        }
      `;

  private updateUserQuery: any = gql`
  mutation updateUser($username: String!, $input: UserInput!) {
    updateUser(username: $username, input: $input) {
      username
      admin
      firstName
    }
  }
  `;

  private changePasswordQuery:any = gql`
  mutation changePassword($username: String!, $currentPassword: String!, $newPassword: String!) {
    changePassword(username: $username, currentPassword: $currentPassword, newPassword: $newPassword)
  }
`;


    updateCurrentUserData() {
      if (localStorage.getItem('currentUser')) {
        this.apollo.watchQuery<UserQueryResponse>({
          query: this.getSelf,
          fetchPolicy: 'network-only'

        }).subscribe(({data}) => {
          this.loading = data.loading;
          this.currentUser = data.getSelf;
          this.userUpdated.emit(data.getSelf);
        });
      }
    }

    public logoutUser() {
      this.currentUser = {firstName: 'Gast', admin: false};
      this.userUpdated.emit(this.currentUser);
    }

    public updateUser(username, password?, firstName?, lastName?) {
      let input = {
        username,
        password,
        firstName,
        lastName
      };
      Object.keys(input).forEach(key => input[key] === undefined && delete input[key])
      console.log(input);

      this.apollo.mutate({
        mutation: this.updateUserQuery,
        variables: {username: username, input: input}
      })
        .toPromise()
        .then(({data}: ApolloQueryResult<any>) => {
          if (data.ApolloError) {
            console.log(data.ApolloError.message);
          }
          console.log(data);

        })
        .catch((errors:any) => {
          console.log('there was an error sending the query', errors);
        });


    }

    public changeUserPassword(username, currentPassword, newPassword) {
      console.log("change:" + username + currentPassword + newPassword);
      return this.apollo.mutate({
        mutation: this.changePasswordQuery,
        variables: {
          username: username,
          currentPassword: currentPassword,
          newPassword: newPassword,
        },
      })
        .toPromise()
        .then(({data}: ApolloQueryResult<any>) => {
        return new Promise((resolve, reject) => {
          resolve(data.changePassword);
        });

        })
        .catch((errors:any) => {
          console.log('there was an error sending the query', errors);
        });
    }
}

