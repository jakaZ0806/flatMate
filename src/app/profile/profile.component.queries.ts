import gql from 'graphql-tag';

/* ---------------QUERIES-------------------- */
const getUserByUsername: any = gql`
query getUser($username: String) {
  user(username: $username) {
    username
    firstName
    lastName,
    email
    groups {
      name
    }
  }
}
`;

const getUserGroupsByUsername: any = gql`
query getUser($username: String) {
  user(username: $username) {
    groups {
      name
      description
      members {
        username
        firstName
        lastName
        }
        categories
      }
  }
}
`;

const getBudgetForUserAndGroup: any = gql`
query getBudget($username: String!, $groupname: String!) {
budget(username: $username, groupname: $groupname)
}
`;

const getAllBudgetsForUser: any = gql`
query getAllBudgets($username: String!) {
  getAllBudgets(username: $username) {
      group {
        name
      }
      budget
  }
}
`;

const queries = {getUserByUsername, getBudgetForUserAndGroup, getUserGroupsByUsername, getAllBudgetsForUser};


/* ---------------------MUTATIONS------------------ */

const updateUser: any = gql`
mutation updateUser($username: String!, $input: UserInput!) {updateUser(username: $username, input: $input) {
    username
    firstName
    lastName
    groups {
      name
      }
    }
}
`;

const addGroup: any = gql`
mutation addGroup($input: GroupInput!) {
  addGroup(input: $input) {
      name
      members {
        username
        }
    }
}
`;


const mutations = {addGroup, updateUser};



export {queries, mutations};
