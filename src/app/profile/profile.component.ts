import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Apollo, ApolloQueryObservable } from 'apollo-angular';
import { ApolloQueryResult} from 'apollo-client';
import * as graphQLFunctions from './profile.component.queries';
import { UserService } from '../shared/services/user.service';
import {ChangePasswordDialogComponent} from '../shared/change-password-dialog.component';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {

    editEnabled = false;
    public getUserQuery: ApolloQueryObservable<any>;
    public currentUsername: String;
    public username: String;
    public firstName: String;
    public lastName: String;
    public email: String;
    public groups: any[] = [];
    public userGroups;
    GroupBudgetQuery;


    displayedColumns = ['name', 'budget'];

    constructor(public apollo: Apollo, public cd: ChangeDetectorRef, private UserService: UserService,
      public dialog: MatDialog, private router: Router, private snackBar: MatSnackBar) { }

    ngOnInit() {
      this.currentUsername = JSON.parse(localStorage.getItem('currentUser')).username;
      this.getUserData();

    }


    public getUserData() {
      this.getUserQuery = this.apollo.watchQuery<any>({
        query: graphQLFunctions.queries.getUserByUsername,
        variables: {
          username: this.currentUsername
        }
      });

      this.getUserQuery.subscribe(({data}) => {
        this.firstName = data.user.firstName;
        this.lastName = data.user.lastName;
        this.username = data.user.username;
        this.email = data.user.email;
        this.groups = data.user.groups;
        this.cd.detectChanges();

        this.GroupBudgetQuery = this.apollo.watchQuery<any>({
          query: graphQLFunctions.queries.getAllBudgetsForUser,
          variables: {
            username: this.currentUsername,
          }
        });

        this.GroupBudgetQuery.subscribe(({data}) => {
          this.groups = data.getAllBudgets;
          this.userGroups = new GroupDataSource(this.groups);
          this.cd.markForCheck();
        });

      });
    }

    edit() {
      this.editEnabled = !this.editEnabled;
      if (!this.editEnabled) {
        this.updateUser();
      }
    }

      public updateUser() {
        this.apollo.mutate({
          mutation: graphQLFunctions.mutations.updateUser,
          variables: {
            'username': this.currentUsername,
            'input': {
              'username': this.username,
              'firstName': this.firstName,
              'lastName': this.lastName,
              'email': this.email,
            }
          },
        })
          .toPromise()
          .then(({data}: ApolloQueryResult<any>) => {
            if (data.ApolloError) {
              console.log(data.ApolloError.message);
            }
            this.getUserQuery.refetch();
            this.UserService.updateCurrentUserData();
          })
          .catch((errors: any) => {
            console.log('there was an error sending the query', errors);
          });
      }


      changePasswordDialog() {
        // open Change PW Form, then success
        const dialogRef = this.dialog.open(ChangePasswordDialogComponent, {disableClose: false, data: {mandatory: false}});
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.UserService.changeUserPassword(this.currentUsername, result.currentPassword, result.newPassword).then((data) => {
              if (data === 'Wrong Password') {
                  alert('Wrong Password!');
                  this.changePasswordDialog();
              } else if (data === 'Success') {
                  this.snackBar.open('Passwort erfolgreich geändert', null, {
                    duration: 2000
                  });
                }
            });
          }
        });
    }

    goToGroup(groupname) {
      this.router.navigate(['/budget/' + groupname]);
    }

  }

class GroupDataSource extends DataSource<any> {
  constructor(private data: any[]) {
    super();
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<any[]> {
    return Observable.of(this.data);
  }

  disconnect() {}
}



