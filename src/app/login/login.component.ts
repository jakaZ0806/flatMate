import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Apollo } from 'apollo-angular';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';

import { AuthenticationService } from '../shared/services/authentication.service';
import { ChangePasswordDialogComponent } from '../shared/change-password-dialog.component';
import { ApolloQueryResult } from "apollo-client";
import gql from 'graphql-tag';
import 'rxjs/add/operator/toPromise';
import { UserService } from '../shared/services/user.service';
import { RegisterDialogComponent } from './register-dialog.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})

export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;
  error = '';

  private changePassword: any = gql`
      mutation changePassword($username: String!, $currentPassword: String!, $newPassword: String!) {
        changePassword(username: $username, currentPassword: $currentPassword, newPassword: $newPassword)
      }
    `;

  private addUser: any = gql`
    mutation addUser($input: UserInput!) {addUser(input: $input) {
        username
        firstName
        lastName
	      }
    }
`;


  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private apollo: Apollo,
    public dialog: MatDialog,
    private UserService: UserService,
    private snackBar: MatSnackBar) {
  }


  ngOnInit() {
    // reset login status
    this.authenticationService.logout();

  }

  login() {
    this.authenticationService.login(this.model.username, this.model.password)
      .then(result => {
        if (result === 'success') {
          // login successful
          this.router.navigate(['/']);
        }
        else if (result === 'changePassword') {
          this.openChangePasswordDialog();
        }

        else {
          // login failed
          this.error = result;
          this.loading = false;
        }

      }
      )
  }

  public register(user) {
    this.apollo.mutate({
      mutation: this.addUser,
      variables: {
        input: {
          username: user.username,
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
          password: user.password
        }
      },
    })
      .toPromise()
      .then(({ data }: ApolloQueryResult<any>) => {
        this.snackBar.open('Registrierung erfolgreich!', 'Willkommen ' + data.addUser.firstName, {
          duration: 5000
        });
        this.model.username = data.addUser.username;

      })
      .catch((errors: any) => {
        console.log('there was an error sending the query', errors);
        alert('Fehler! Bitte nochmal versuchen!');
      });
  }

  openRegisterDialog() {
    //open Change PW Form, then success
    let dialogRef = this.dialog.open(RegisterDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.register(result);
      }
    });

  }

  openChangePasswordDialog() {
    //open Change PW Form, then success
    let dialogRef = this.dialog.open(ChangePasswordDialogComponent, { disableClose: true, data: { mandatory: true } });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.UserService.changeUserPassword(this.model.username, result.currentPassword, result.newPassword).then((data) => {
          if (data === 'Wrong Password') {
            alert('Wrong Password!');
            this.openChangePasswordDialog();
          } else if (data === 'Success') {
            this.router.navigate(['/']);
          }
          else {
          }
        });
      }
    });

  }
}
