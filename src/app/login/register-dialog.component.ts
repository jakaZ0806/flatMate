import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormControl, Validators  } from '@angular/forms';

@Component({
  selector: 'app-register-dialog',
  templateUrl: 'register-dialog.component.html',
  styleUrls: ['register-dialog.component.css'],
  providers: []
})



export class RegisterDialogComponent implements OnInit {

username = new FormControl('', [Validators.required]);
firstName  = new FormControl('', [Validators.required]);
lastName = new FormControl('', [Validators.required]);
email = new FormControl('', [Validators.required, Validators.email]);
password = new FormControl('', [Validators.required, Validators.minLength(5)]);

  constructor(public dialogRef: MatDialogRef<RegisterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }
  closeDialogAbort() {
    this.dialogRef.close();
  }

  closeDialogSuccess() {
    const user = {
      username: this.username.value,
      firstName: this.firstName.value,
      lastName: this.lastName.value,
      email: this.email.value,
      password: this.password.value
    }
    this.dialogRef.close(user);
  }

  checkEmailError() {
    return this.email.hasError('required') ? 'E-Mail Adresse eingeben!' :
    this.email.hasError('email') ? 'E-Mail Adresse ungültig' :
    null;
  }

  checkPasswordError() {
    return this.password.hasError('required') ? 'Passwort eingeben!' :
    this.password.hasError('minlength') ? 'Mindestens 5 Zeichen!' :
    null;
  }

  checkUsernameError() {
    return this.username.hasError('required') ? 'Benutzernamen eingeben!' :
    null;
  }

  checkFirstNameError() {
    return this.firstName.hasError('required') ? 'Vornamen eingeben!' :
    null;
  }

  checkLastNameError() {
    return this.lastName.hasError('required') ? 'Nachnamen eingeben!' :
    null;
  }


}
