import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ApolloModule } from 'apollo-angular';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { routing } from './shared/routes';
import { getClient } from './shared/apolloClient';

import { AuthGuard } from './auth-guard';
import { AuthenticationService} from './shared/services/authentication.service';
import { UserService} from './shared/services/user.service';

import { AppComponent } from './app.component';
import { BudgetComponent } from './budget/budget.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AdminpanelComponent } from './adminpanel/adminpanel.component';
import { AdduserDialogComponent } from './adminpanel/adduser-dialog.component';
import { AddgroupDialogComponent } from './management/addgroup-dialog.component';
import { AddusertogroupDialogComponent } from './management/group-management/addusertogroup-dialog.component';
import { AddcategorytogroupDialogComponent } from './management/group-management/addcategorytogroup-dialog.component';
import { AddgrouptouserDialogComponent } from './adminpanel/addgrouptouser-dialog.component';
import { KeysPipe } from './shared/keyspipe.pipe';
import { AddBillDialogComponent } from './budget/add-bill-dialog.component';
import {ConfirmdeleteDialogComponent} from './shared/confirmDelete-dialog.component';
import {ChangePasswordDialogComponent} from './shared/change-password-dialog.component';

import { MatCardModule, MatFormFieldModule, MatButtonModule,
  MatMenuModule, MatInputModule, MatSelectModule, MatTabsModule, MatSnackBarModule,
  MatSnackBar, MatDialogModule, MatToolbarModule, MatIconModule, MatCheckboxModule,
  MatListModule, MatTableModule, MatTooltipModule, MatAutocompleteModule } from '@angular/material';
import { ProfileComponent } from './profile/profile.component';
import { BudgetOverviewComponent } from './budget-overview/budget-overview.component';
import { ManagementComponent } from './management/management.component';
import { GroupManagementComponent } from './management/group-management/group-management.component';
import { RegisterDialogComponent } from './login/register-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    BudgetComponent,
    NavbarComponent,
    LoginComponent,
    AdminpanelComponent,
    AdduserDialogComponent,
    AddgroupDialogComponent,
    AddusertogroupDialogComponent,
    AddgrouptouserDialogComponent,
    AddcategorytogroupDialogComponent,
    KeysPipe,
    AddBillDialogComponent,
    ConfirmdeleteDialogComponent,
    ChangePasswordDialogComponent,
    ProfileComponent,
    BudgetOverviewComponent,
    ManagementComponent,
    GroupManagementComponent,
    RegisterDialogComponent
  ],
  entryComponents: [
    AdduserDialogComponent,
    AddgroupDialogComponent,
    AddusertogroupDialogComponent,
    AddgrouptouserDialogComponent,
    AddBillDialogComponent,
    AddcategorytogroupDialogComponent,
    ConfirmdeleteDialogComponent,
    ChangePasswordDialogComponent,
    RegisterDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing,
    ApolloModule.forRoot(getClient),
    BrowserAnimationsModule,
    NgxChartsModule,
    MatCardModule, MatFormFieldModule, MatButtonModule, MatMenuModule, MatInputModule,
    MatSelectModule, MatTabsModule, MatSnackBarModule, MatDialogModule, MatToolbarModule,
    MatIconModule, MatCheckboxModule, MatListModule, MatTableModule, MatTooltipModule,
    MatAutocompleteModule
  ],
  providers: [
    AuthGuard,
    AuthenticationService,
    UserService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
