import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthenticationService } from '../shared/services/authentication.service';
import { Router } from '@angular/router';
import { UserService } from '../shared/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, AfterViewInit {

  constructor(private authenticationService: AuthenticationService, private router: Router, private userService: UserService) { }

  public currentUser = {firstName: "Gast", admin: false, superUser: false};

  ngOnInit() {
    this.userService.userUpdated.subscribe(
      (user) => {
        this.currentUser = user;
      }
    );
  }

  ngAfterViewInit() {
    this.userService.updateCurrentUserData();
}


  public logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
