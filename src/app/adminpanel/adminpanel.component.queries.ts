/**
 * Created by Luki on 01.05.17.
 */
import gql from "graphql-tag";


/*---------------QUERIES--------------------*/
const getAllGroups: any = gql`
    query getGroups {
      groups {
          name
          categories
          members {
              username
            }
	      }
    }
`;

const getAllUsers: any = gql`
    query getUsers {
      users {
          username
          firstName
          lastName
          groups {
            name
            }
	      }
    }
`;

const queries = {getAllUsers, getAllGroups};


/*---------------------MUTATIONS------------------*/
const addUser: any = gql`
    mutation addUser($input: UserInput!) {addUser(input: $input) {
        username
        firstName
        lastName
        groups {
          name
          }
	      }
    }
`;

const addGroup: any = gql`
    mutation addGroup($input: GroupInput!) {
      addGroup(input: $input) {
          name
          members {
            username
            }
	      }
    }
`;

const deleteUser: any = gql `
    mutation deleteUser($username: String!) {
          deleteUser(username: $username) {
              username
            }
        }
`;

const deleteGroup: any = gql `
    mutation deleteGroup($groupname: String!) {
          deleteGroup(groupname: $groupname) {
              name
            }
        }
`;

const addUserToGroup: any = gql `
      mutation addUserToGroup($username: String!, $groupname: String!) {
          addUserToGroup(username: $username, groupname: $groupname) {
             username
             groups {
                name
               }
          }
        }

`;

const deleteCategory: any = gql `
      mutation deleteCategory($category: String!, $group: String!) {
          deleteCategory(category: $category, group: $group)
      }

`;

const addCategory: any = gql `
      mutation addCategory($category: String!, $group: String!) {
          addCategory(category: $category, group: $group)
      }

`;


const mutations = {addUser, addGroup, deleteUser, deleteGroup, addUserToGroup, deleteCategory, addCategory};



export {queries, mutations};
