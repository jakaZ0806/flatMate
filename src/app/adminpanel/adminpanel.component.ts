import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { ApolloQueryResult } from 'apollo-client';
import * as graphQLFunctions from './adminpanel.component.queries';
import { MatDialog, MatDialogRef } from '@angular/material';
import {AdduserDialogComponent} from './adduser-dialog.component';
import {AddgroupDialogComponent} from '../management/addgroup-dialog.component';
import {AddusertogroupDialogComponent} from '../management/group-management/addusertogroup-dialog.component';
import {AddcategorytogroupDialogComponent} from '../management/group-management/addcategorytogroup-dialog.component';
import {UserService} from '../shared/services/user.service';
import {AddgrouptouserDialogComponent} from './addgrouptouser-dialog.component';
import {ConfirmdeleteDialogComponent} from '../shared/confirmDelete-dialog.component';


interface UsersQueryResponse {
  users;
  loading;
}

interface GroupsQueryResponse {
  groups;
  loading;
}




@Component({
  selector: 'app-adminpanel',
  templateUrl: './adminpanel.component.html',
  styleUrls: ['./adminpanel.component.css']
})
export class AdminpanelComponent implements OnInit {

  loading: boolean;
  users: any;
  groups: any;
  category: any;

  constructor(public apollo: Apollo, public dialog: MatDialog, public userService: UserService) { }

  openNewUserDialog() {
    const dialogRef = this.dialog.open(AdduserDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addUser(result);
      }
    });
  }

  openNewGroupDialog() {
    const dialogRef = this.dialog.open(AddgroupDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addGroup(result);
      }
    });
  }


  openAddUserToGroupDialog(group) {
    const dialogRef = this.dialog.open(AddusertogroupDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addUserToGroup(result, group);
      }
    });
  }

  openAddGroupToUserDialog(user) {
    const dialogRef = this.dialog.open(AddgrouptouserDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addUserToGroup(user, result);
      }
    });
  }

  openAddCategoryToGroupDialog(group) {
    const dialogRef = this.dialog.open(AddcategorytogroupDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addCategoryToGroup(result, group);
      }
    });
  }

  openConfirmDeleteDialog() {
    const dialogRef = this.dialog.open(ConfirmdeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      return result;
    });
  }

  ngOnInit() {

    this.apollo.watchQuery<UsersQueryResponse>({
      query: graphQLFunctions.queries.getAllUsers,
    }).subscribe(({data}) => {
      this.loading = data.loading;
      this.users = data.users;
    });

    this.apollo.watchQuery<GroupsQueryResponse>({
      query: graphQLFunctions.queries.getAllGroups,
    }).subscribe(({data}) => {
      this.loading = data.loading;
      this.groups = data.groups;
    });


  }

  public addUser(user) {
    this.apollo.mutate({
      mutation: graphQLFunctions.mutations.addUser,
      variables: {
        'input': {
          'username': user.username,
          'firstName': user.firstName,
          'lastName': user.lastName,
          'email': user.email,
          'admin': user.admin,
          'password': user.password
        }
      },
    })
      .toPromise()
      .then(({data}: ApolloQueryResult<any>) => {
        if (data.ApolloError) {
          console.log(data.ApolloError.message);
        }

      })
      .catch((errors:any) => {
        console.log('there was an error sending the query', errors);
      });
  }

  public addGroup(group) {
    this.apollo.mutate({
      mutation: graphQLFunctions.mutations.addGroup,
      variables: {
        'input': {
          'name': group.name,
          'description': group.description,
          'initialMember': group.initialMember
        }
      },
    })
      .toPromise()
      .then(({data}: ApolloQueryResult<any>) => {
        if (data.ApolloError) {
          console.log(data.ApolloError.message);
        }

      })
      .catch((errors:any) => {
        console.log('there was an error sending the query', errors);
      });
  }

  public deleteUser(username) {
    const dialogRef = this.dialog.open(ConfirmdeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.apollo.mutate({
          mutation: graphQLFunctions.mutations.deleteUser,
          variables: {
            'username': username
          }
        })
          .toPromise()
          .then(({data}: ApolloQueryResult<any>) => {
            if (data.ApolloError) {
              console.log(data.ApolloError.message);
            }


          })
          .catch((errors:any) => {
            console.log('there was an error sending the query', errors);
          });
      }
    })
  }

  public deleteGroup(groupname) {
    const dialogRef = this.dialog.open(ConfirmdeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.apollo.mutate({
          mutation: graphQLFunctions.mutations.deleteGroup,
          variables: {
            'groupname': groupname
          }
        })
          .toPromise()
          .then(({data}: ApolloQueryResult<any>) => {
            if (data.ApolloError) {
              console.log(data.ApolloError.message);
            }


          })
          .catch((errors:any) => {
            console.log('there was an error sending the query', errors);
          });
      }
    });
  }

  public addUserToGroup(username, groupname) {
    this.apollo.mutate({
      mutation: graphQLFunctions.mutations.addUserToGroup,
      variables: {
        username,
        groupname
      }
    })
      .toPromise()
      .then(({data}: ApolloQueryResult<any>) => {
        if (data.ApolloError) {
          console.log(data.ApolloError.message);
        }


      })
      .catch((errors: any) => {
        console.log('there was an error sending the query', errors);
      });
  }

  public addCategoryToGroup(category, group) {
    this.apollo.mutate({
      mutation: graphQLFunctions.mutations.addCategory,
      variables: {
        category,
        group
      }
    })
      .toPromise()
      .then(({data}: ApolloQueryResult<any>) => {
        if (data.ApolloError) {
          console.log(data.ApolloError.message);
        }


      })
      .catch((errors: any) => {
        console.log('there was an error sending the query', errors);
      });
  }

  public deleteCategory(category, group) {
    const dialogRef = this.dialog.open(ConfirmdeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.apollo.mutate({
          mutation: graphQLFunctions.mutations.deleteCategory,
          variables: {
            category,
            group
          }
        })
          .toPromise()
          .then(({data}: ApolloQueryResult<any>) => {
            if (data.ApolloError) {
              console.log(data.ApolloError.message);
            }


          })
          .catch((errors: any) => {
            console.log('there was an error sending the query', errors);
          });
      }
      });
  }



}
