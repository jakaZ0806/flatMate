import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-adduser-dialog',
  template: `
    <h1 matDialogTitle>Add User</h1>
    <mat-dialog-content>
       <mat-form-field>
        <input matInput type="text" placeholder="username" #username value="maxmustermann">
      </mat-form-field>
      <mat-form-field>
        <input matInput type="text" placeholder="First Name" #firstName value="Max">
      </mat-form-field>
      <mat-form-field>
        <input matInput type="text" placeholder="Last Name" #lastName value="Mustermann">
      </mat-form-field>
      <mat-form-field>
        <input matInput type="email" placeholder="E-Mail" #email value="max.mustermann@gmail.com">
      </mat-form-field>
      <mat-form-field>
        <input matInput type="text" placeholder="admin" #admin value="false">
      </mat-form-field>
      <mat-form-field>
        <input matInput type="password" placeholder="password" #pass value="maxisawesome">
      </mat-form-field>
    </mat-dialog-content>
    <mat-dialog-actions>
      <button mat-raised-button color="primary" (click)="closeDialogSuccess(username.value, firstName.value, lastName.value, email.value, admin.value, pass.value)">
        Hinzufügen
      </button>
        <button mat-raised-button color="primary" (click)="closeDialogAbort()">
        Abbrechen
      </button>

    </mat-dialog-actions>
  `,
  styles: []
})
export class AdduserDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AdduserDialogComponent>) { }

  closeDialogAbort() {
    this.dialogRef.close();
  }

  closeDialogSuccess(username, firstName, lastName, email, admin, password) {
    this.dialogRef.close(
      {'username': username,
       'firstName': firstName,
       'lastName': lastName, 'email': email,
       'admin': (admin === 'true'),
       'password': password
      });
  }

  ngOnInit() {
  }

}
