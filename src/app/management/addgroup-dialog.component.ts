import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-addgroup-ialog',
  template: `
    <h1 matDialogTitle>Neue Gruppe</h1>
    <mat-dialog-content>
    <mat-form-field>
        <input matInput type="text" placeholder="Gruppenname" #name value="" required>
    </mat-form-field><br>
    <mat-form-field>
        <input matInput type="text" placeholder="Beschreibung" #description value="" required>
    </mat-form-field><br>
    <mat-form-field matTooltip="Leer lassen, wenn du es selber sein möchtest.">
        <input matInput type="text" placeholder="Erstes Mitglied" #member value="">
        <mat-hint align="start"><strong>ist auch Gruppenverwalter</strong> </mat-hint>
    </mat-form-field>
    </mat-dialog-content>
    <mat-dialog-actions>
      <button mat-raised-button (click)="closeDialogSuccess(name.value, description.value, member.value)">
        Hinzufügen
      </button>
        <button mat-raised-button (click)="closeDialogAbort()">
        Abbrechen
      </button>
    </mat-dialog-actions>
  `,
  styles: []
})
export class AddgroupDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AddgroupDialogComponent>) { }

  closeDialogAbort() {
    this.dialogRef.close();
  }

  closeDialogSuccess(name, description, initialMember) {
    this.dialogRef.close({'name': name, 'description': description, 'initialMember': initialMember});
  }

  ngOnInit() {
  }

}
