import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-addcategorytogroup-dialog',
  template: `
    <h1 matDialogTitle>Add Group</h1>
    <mat-dialog-content>
    <mat-form-field>
        <input matInput type="text" placeholder="Kategorie" #category value="">
      </mat-form-field>
    </mat-dialog-content>
    <mat-dialog-actions>
      <button mat-raised-button color="primary" (click)="closeDialogSuccess(category.value)">
        Hinzufügen
      </button>
        <button mat-raised-button (click)="closeDialogAbort()">
        Abbrechen
      </button>
    </mat-dialog-actions>
  `,
  styles: []
})
export class AddcategorytogroupDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AddcategorytogroupDialogComponent>) { }

  closeDialogAbort() {
    this.dialogRef.close();
  }

  closeDialogSuccess(name) {
    this.dialogRef.close(name);
  }

  ngOnInit() {
  }

}
