import { Component, OnInit } from '@angular/core';
import {Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Apollo, ApolloQueryObservable } from 'apollo-angular';
import { ApolloQueryResult} from 'apollo-client';
import * as graphQLFunctions from './group-management.component.queries';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-addusertogroup-dialog',
  template: `
    <h1 matDialogTitle>Add Group</h1>
    <mat-dialog-content class="adduser-form">
    <mat-form-field class="full-width">
    <input matInput placeholder="User" [matAutocomplete]="auto" [formControl]="userCtrl">
    <mat-autocomplete #auto="matAutocomplete">
      <mat-option *ngFor="let user of filteredUsers | async" [value]="user.username">
        <span>{{ user.username }}</span> |
        <small>{{user.firstName}} {{user.lastName}}</small>
      </mat-option>
    </mat-autocomplete>
  </mat-form-field>
    </mat-dialog-content>
    <mat-dialog-actions>
      <button mat-raised-button (click)="closeDialogSuccess()">
        Hinzufügen
      </button>
        <button mat-raised-button (click)="closeDialogAbort()">
        Abbrechen
      </button>
    </mat-dialog-actions>
  `,
  styles: [
    `.adduser-form {
      min-width: 250px;
      max-width: 500px;
      width: 100%;
    }
    .full-width {
      width: 100%;
    }
`
  ]
})
export class AddusertogroupDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AddusertogroupDialogComponent>, @Inject(MAT_DIALOG_DATA) data: any, private apollo: Apollo) {
    this.users = [];
    this.userCtrl = new FormControl();
   }

  userQuery;
  users;
  userCtrl: FormControl;
  filteredUsers: Observable<any[]>;
  
  closeDialogAbort() {
    this.dialogRef.close();
  }

  closeDialogSuccess() {
    this.dialogRef.close(this.userCtrl.value);
  }

  ngOnInit() {
    this.userQuery = this.apollo.watchQuery<any>({
      query: graphQLFunctions.queries.getAllUsers      
    });
  
    this.userQuery.subscribe(({data}) => {
      this.users = data.users;
      this.filteredUsers = this.userCtrl.valueChanges
          .startWith(null)
          .map(user => user ? this.filterUsers(user) : this.users.slice());
    });
  }

  filterUsers(username: string) {
    return this.users.filter(user =>
      user.username.toLowerCase().indexOf(username.toLowerCase()) === 0);
  }
}
