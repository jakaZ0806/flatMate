import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as graphQLFunctions from './group-management.component.queries';
import { Apollo, ApolloQueryObservable } from 'apollo-angular';
import { ApolloQueryResult} from 'apollo-client';
import { UserService } from '../../shared/services/user.service';
import {DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {AddusertogroupDialogComponent} from './addusertogroup-dialog.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { AddcategorytogroupDialogComponent } from './addcategorytogroup-dialog.component';

@Component({
  selector: 'app-group-management',
  templateUrl: './group-management.component.html',
  styleUrls: ['./group-management.component.css']
})
export class GroupManagementComponent implements OnInit {
  editEnabled: Boolean = false;
  groupname;
  group = {name: 'dummy', description: 'dummyDescription', moderators: [], members: [], categories: []};
  name;
  description;
  groupQuery;
  currentUser = this.userService.currentUser;
  memberDataSource;
  categoryDataSource;
  windowWidth = window.innerWidth;
  displayedColumnsMembers = ['username', 'firstName', 'lastName', 'isModerator', 'actions'];

  constructor(private route: ActivatedRoute, private apollo: Apollo, private userService: UserService, private dialog: MatDialog,
  private snackBar: MatSnackBar) { }

  @HostListener('window:resize', ['$event'])
  resize(event) {
    this.windowWidth = window.innerWidth;
    this.setDisplayedColumns();
  }

  ngOnInit() {
    this.setDisplayedColumns();
    this.route.params.subscribe(params => {
      this.groupname = params.group;
    });
    this.currentUser = this.userService.currentUser;
    this.userService.userUpdated.subscribe(
      (user) => {
        this.currentUser = user;
      }
    );
    this.initialQuery();
  }

  initialQuery() {
  this.groupQuery = this.apollo.watchQuery<any>({
    query: graphQLFunctions.queries.getGroup,
    variables: {
      name: this.groupname,
    }
  });

  this.groupQuery.subscribe(({data}) => {
    this.group = data.group;
    this.name = data.group.name;
    this.description = data.group.description;
    this.memberDataSource = new MemberDataSource(this.group.members);
  });
}

  edit() {
    this.editEnabled = !this.editEnabled;
    if (!this.editEnabled) {
      this.updateGroup();
    }
  }


  checkRoleMembership(username) {
    if (this.group.moderators.find(x => x.username === username)) {
        return true;
    } else {
      return false;
    }
  }

  openAddUserToGroupDialog() {
    const dialogRef = this.dialog.open(AddusertogroupDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addUserToGroup(result);
      }
    });
  }

  setDisplayedColumns() {
    if (this.windowWidth < 820) {
      this.displayedColumnsMembers = ['username', 'actions'];
        } else {
          this.displayedColumnsMembers = ['username', 'firstName', 'lastName', 'isModerator', 'actions'];
        }
  }

  deleteCategory(category) {
    // console.log(category);
  }

  public openAddCategoryDialog() {
    const dialogRef = this.dialog.open(AddcategorytogroupDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addCategory(result);
      }
    });

  }

  public addCategory(category) {
    this.apollo.mutate({
      mutation: graphQLFunctions.mutations.addCategory,
      variables: {
        category: category,
        group: this.group.name
      }
    })
      .toPromise()
      .then(({data}: ApolloQueryResult<any>) => {
        this.snackBar.open('Kategorie Hinzugefügt', data.addCategory, {
          duration: 2000
        });
        this.groupQuery.refetch();


      })
      .catch((errors: any) => {
        console.log('there was an error sending the query', errors);
      });
  }

  public addUserToGroup(username) {
    this.apollo.mutate({
      mutation: graphQLFunctions.mutations.addUserToGroup,
      variables: {
        username,
        groupname: this.group.name
      }
    })
      .toPromise()
      .then(({data}: ApolloQueryResult<any>) => {
        console.log(data);
        this.snackBar.open('Benutzer Hinzugefügt', data.addUserToGroup.username, {
          duration: 2000
        });
        this.groupQuery.refetch();


      })
      .catch((errors: any) => {
        console.log('there was an error sending the query', errors);
      });
  }

  public promoteToMod(username) {
    this.apollo.mutate({
      mutation: graphQLFunctions.mutations.addModerator,
      variables: {
        username,
        groupname: this.group.name
      }
    })
      .toPromise()
      .then(({data}: ApolloQueryResult<any>) => {
        this.snackBar.open('Zu Moderator befördert:', data.addModerator, {
          duration: 2000
        });
        this.groupQuery.refetch();


      })
      .catch((errors: any) => {
        console.log('there was an error sending the query', errors);
      });
  }

  public updateGroup() {
    this.apollo.mutate({
      mutation: graphQLFunctions.mutations.updateGroup,
      variables: {
        input: {
          name: this.name,
          description: this.description
        }
      },
    })
      .toPromise()
      .then(({data}: ApolloQueryResult<any>) => {
        this.snackBar.open('Bearbeiten Erfolgreich!', null, {
          duration: 2000
        });
        this.groupQuery.refetch();

      })
      .catch((errors: any) => {
        console.log('there was an error sending the query', errors);
      });
  }

}

class MemberDataSource extends DataSource<any> {
  constructor(private data: any[]) {
    super();
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<any[]> {
    return Observable.of(this.data);
  }

  disconnect() {}

}


