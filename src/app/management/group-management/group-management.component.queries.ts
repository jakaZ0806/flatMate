import gql from 'graphql-tag';

const getGroup: any = gql`
query getGroup($name: String!) {
  group(name: $name) {
      name
      description
      members {
        username
        firstName
        lastName
        }
      categories
      moderators {
        username
        firstName
        lastName
      }
  }
}
`;

const getAllUsers: any = gql`
query getAllUsers {
  users {
        username
        firstName
        lastName
  }
}
`;


const queries = {getGroup, getAllUsers};


const deleteGroup: any = gql `
mutation deleteGroup($groupname: String!) {
      deleteGroup(groupname: $groupname) {
          name
        }
    }
`;

const updateGroup: any = gql`
mutation updateGroup($input: GroupInput!) {
  updateGroup(input: $input) {
      name
      description
    }
}
`;

const addUserToGroup: any = gql `
mutation addUserToGroup($username: String!, $groupname: String!) {
    addUserToGroup(username: $username, groupname: $groupname) {
       username
       firstName
       lastName
       groups {
         name
       }
    }
  }

`;

const addCategory: any = gql `
mutation addCategory($category: String!, $group: String!) {
    addCategory(category: $category, group: $group)
}

`;

const addModerator: any = gql `
mutation addModerator($groupname: String!, $username: String!) {
    addModerator(groupname: $groupname, username: $username)
}

`;


const mutations = {deleteGroup, addUserToGroup, addCategory, addModerator, updateGroup};

export {queries, mutations};
