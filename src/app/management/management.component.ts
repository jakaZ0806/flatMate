import { Component, OnInit, ChangeDetectorRef, HostListener } from '@angular/core';
import { Apollo, ApolloQueryObservable } from 'apollo-angular';
import { ApolloQueryResult } from 'apollo-client';
import * as graphQLFunctions from './management.component.queries';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {DataSource} from '@angular/cdk/collections';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { UserService } from '../shared/services/user.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { AddgroupDialogComponent } from './addgroup-dialog.component';
import { ConfirmdeleteDialogComponent } from '../shared/confirmDelete-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', visibility: 'hidden'})),
      state('expanded', style({height: '*', visibility: 'visible'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ManagementComponent implements OnInit {
  getGroupSummaryQuery;
  currentUser = this.userService.currentUser;
  groupSummary = [];
  groupSummaryDataSource;
  expandedElement;

  windowWidth: number = window.innerWidth;

  displayedColumns = ['name', 'numberOfUsers', 'numberOfCategories', 'isMember', 'isModerator'];

  isExpansionDetailRow = (row: any) => row.hasOwnProperty('detailRow');

  constructor(private apollo: Apollo, private cd: ChangeDetectorRef, private userService: UserService,
    private dialog: MatDialog, private snackBar: MatSnackBar, private router: Router) { }

    @HostListener('window:resize', ['$event'])
    resize(event) {
      this.windowWidth = window.innerWidth;
      this.setDisplayedColumns();
    }

  ngOnInit() {
    this.setDisplayedColumns();
    this.currentUser = this.userService.currentUser;
    this.userService.userUpdated.subscribe(
      (user) => {
        this.currentUser = user;
      }
    );
    this.getGroupSummary();
  }

  setDisplayedColumns() {
    if (this.windowWidth < 820) {
      this.displayedColumns = ['name', 'actions'];
        } else {
          this.displayedColumns = ['name', 'numberOfUsers', 'numberOfCategories', 'isMember', 'isModerator', 'actions'];
        }
  }

  public getGroupSummary() {
    this.getGroupSummaryQuery = this.apollo.watchQuery<any>({
      query: graphQLFunctions.queries.getAllGroups,
      variables: {
        username: this.currentUser.username
      }
    });
    this.getGroupSummaryQuery.subscribe(({data}) => {
      this.groupSummary = data.managedGroups;
      this.groupSummaryDataSource = new GroupDataSource(this.groupSummary);
      this.cd.markForCheck();
    });
    }

    changeExpandedElement(row) {
      if (this.expandedElement === row) {
        this.expandedElement = null;
      } else {
        this.expandedElement = row;
      }
    }

    checkRoleMembership(role) {
      if (role.find(x => x.username === this.currentUser.username)) {
        return true;
    } else {
      return false;
    }
  }

  openNewGroupDialog() {
    const dialogRef = this.dialog.open(AddgroupDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addGroup(result);
      }
    });
  }

  public addGroup(group) {
    this.apollo.mutate({
      mutation: graphQLFunctions.mutations.addGroup,
      variables: {
        'input': {
          'name': group.name,
          'description': group.description,
          'initialMember': group.initialMember
        }
      },
    })
      .toPromise()
      .then(({data}: ApolloQueryResult<any>) => {
        this.snackBar.open('Neue Gruppe erstellt', data.addGroup.name, {
          duration: 2000
        });
        this.getGroupSummaryQuery.refetch();

      })
      .catch((errors: any) => {
        console.log('there was an error sending the query', errors);
      });
  }

  public deleteGroup(groupname) {
    const dialogRef = this.dialog.open(ConfirmdeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.apollo.mutate({
          mutation: graphQLFunctions.mutations.deleteGroup,
          variables: {
            'groupname': groupname
          }
        })
          .toPromise()
          .then(({data}: ApolloQueryResult<any>) => {
            this.snackBar.open('Gruppe gelöscht', data.deleteGroup.name, {
              duration: 2000
            });
            this.getGroupSummaryQuery.refetch();


          })
          .catch((errors: any) => {
            console.log('there was an error sending the query', errors);
          });
      }
    });
  }

  editGroup(groupname) {
    this.router.navigate(['/management/' + groupname]);
  }
}

class GroupDataSource extends DataSource<any> {
  constructor(private data: any[]) {
    super();
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<any[]> {
    const rows = [];
    this.data.forEach(element => rows.push(element, {detailRow: true, element}));
    return Observable.of(rows);
  }

  disconnect() {}
}
