import gql from 'graphql-tag';

const getUserSummary: any = gql`
query getUserSummary($username: String!) {
  getUserSummary(username: $username) {
      groupBudgets {
        group {
          name
          members {
            firstName
            username
          }
          description
          categories
          moderators {
              firstName
              username
          }
        }
        budgets {
          budget
          user {
            firstName
            username
          }
        }
        ownBudget {
          user {
            username
          }
          budget
        }
      }
  }
}
`;

const getAllGroups: any = gql`
query getManagedGroups($username: String!) {
  managedGroups(username: $username) {
    name
    members {
      firstName
      lastName
      username
    }
    description
    categories
    moderators {
        firstName
        username
    }
    }
}
`;

const queries = {getUserSummary, getAllGroups};


const addGroup: any = gql`
mutation addGroup($input: GroupInput!) {
  addGroup(input: $input) {
      name
      members {
        username
        }
    }
}
`;

const deleteGroup: any = gql `
mutation deleteGroup($groupname: String!) {
      deleteGroup(groupname: $groupname) {
          name
        }
    }
`;


const mutations = {addGroup, deleteGroup};

export {queries, mutations};
