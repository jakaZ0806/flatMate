import { Component, OnInit, HostListener, AfterViewInit, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Apollo, ApolloQueryObservable } from 'apollo-angular';
import { ApolloQueryResult } from 'apollo-client';
import * as graphQLFunctions from './budget.component.queries';
import { KeysPipe } from '../shared/keyspipe.pipe';
import { MatSnackBar } from '@angular/material';
import { AddBillDialogComponent } from './add-bill-dialog.component';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ConfirmdeleteDialogComponent } from '../shared/confirmDelete-dialog.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { DecimalPipe } from '@angular/common';

interface BudgetQueryResponse {
  budget;
  loading;
}

interface UserQueryResponse {
  user;
  loading;
}

interface BillsQueryResponse {
  bills;
  loading;
}

interface GroupBudgetQueryResponse {
  groupBudget;
  loading;
}

@Component({
  selector: 'app-budget',
  templateUrl: './budget.component.html',
  styleUrls: ['./budget.component.css'],
  providers: [KeysPipe, DecimalPipe],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', visibility: 'hidden'})),
      state('expanded', style({height: '*', visibility: 'visible'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])
  ]
})


export class BudgetComponent implements OnInit, AfterViewInit {

  @ViewChild('name') nameInputField: ElementRef;

  loading: boolean;
  user: any;

  displayedColumns;
  // --------------Pie Chart------------------
  view: any[];
  showLegend = false;
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA', '#4286f4', '#000000', '#ccffff']
  };
  // pie
  showLabels = true;
  explodeSlices = false;
  // -----------------------------------

  public monthNames = ['Jan', 'Feb', 'Mrz', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'];

  windowWidth: number = window.innerWidth;

  public currentMonth: number;
  public currentYear: number;

  public currentUsername;
  public currentBudget;
  public currentGroupBudget;
  public currentGroupCategorySummary;
  public currentGroupAndMonthCategorySummary;
  public userGroups: [any] = [{ 'name': 'DummyGroup', 'description': 'DummyGroup' }];
  public currentUserGroup: any;
  public userHasGroups: Boolean = false;

  public budgetQuery: ApolloQueryObservable<any>;
  public groupBudgetQuery: ApolloQueryObservable<any>;
  public groupCategorySummaryQuery: ApolloQueryObservable<any>;
  public groupAndDateBudgetQuery;
  public billsQuery: ApolloQueryObservable<any>;
  public userGroupsQuery: ApolloQueryObservable<any>;
  payer;
  expandedElement;
  category;
  cost;
  zweck;
  currentTotalExpenses;

  bills: any[] = [];
  billsDataSource;

  detailsVisible = false;


  consumerCheckboxes = [];


  isExpansionDetailRow = (row: any) => row.hasOwnProperty('detailRow');

  constructor(public apollo: Apollo, public cd: ChangeDetectorRef, public snackBar: MatSnackBar, public dialog: MatDialog,
    private route: ActivatedRoute, private location: Location, private router: Router) {
  }

  setDisplayedColumns() {
    if (this.windowWidth < 820) {
      this.displayedColumns = ['name', 'sum'];
        } else {
          this.displayedColumns = ['name', 'sum', 'paidBy', 'date', 'actions'];
        }
  }

  ngOnInit() {
    this.setDisplayedColumns();
    this.currentMonth = new Date(Date.now()).getMonth();
    this.currentYear = new Date(Date.now()).getFullYear();
    this.currentUsername = JSON.parse(localStorage.getItem('currentUser')).username;
    this.initialQuery();


  }

  public initialQuery() {
    this.userGroupsQuery = this.apollo.watchQuery<UserQueryResponse>({
      query: graphQLFunctions.queries.getUserGroupsByUsername,
      variables: {
        username: this.currentUsername,
      }
    });


    this.userGroupsQuery.subscribe(({ data }) => {
      this.loading = data.loading;
      this.userGroups = data.user.groups;

      if (this.userGroups.length === 0) {
        this.userHasGroups = false;
      } else {
        this.userHasGroups = true;
        if (!this.currentUserGroup) {
          this.route.params.subscribe(params => {
            if (this.setCurrentUserGroup(params.group)) {
              this.initializeBudgetQuery();
              this.initializeBillsQuery();
              this.initializeGroupBudgetQuery();
              this.initializeGroupCategorySummaryQuery();
              this.initializeGroupAndDateCategorySummaryQuery();
              this.consumerCheckboxes = [];
              for (const i in this.currentUserGroup.members) {
                this.consumerCheckboxes.push(true);
              }
              this.cd.markForCheck();
            }
          });

        }
      }
    });

  }

  public initializeBudgetQuery() {
    this.budgetQuery = this.apollo.watchQuery<BudgetQueryResponse>({
      query: graphQLFunctions.queries.getBudgetForUserAndGroup,
      variables: {
        username: this.currentUsername,
        groupname: this.currentUserGroup.name
      }
    });

    this.budgetQuery.subscribe(({ data }) => {
      this.loading = data.loading;
      this.currentBudget = data.budget;
      this.cd.markForCheck();
    });

  }

  public initializeBillsQuery() {
    this.billsQuery = this.apollo.watchQuery<BillsQueryResponse>({
      query: graphQLFunctions.queries.getBillsForGroupAndDate,
      variables: {
        groupname: this.currentUserGroup.name,
        from: new Date(this.currentYear, this.currentMonth, 1, 0, 0),
        to: new Date(this.currentYear, this.currentMonth + 1, new Date(this.currentYear, this.currentMonth + 1, 1).getDate())
      }
    });

    this.billsQuery.subscribe(({ data }) => {
      this.bills = data.billsByGroup;
      this.billsDataSource = new BillsDataSource(this.bills);
      this.cd.markForCheck();
    });

  }

  public initializeGroupBudgetQuery() {
    this.groupBudgetQuery = this.apollo.watchQuery<GroupBudgetQueryResponse>({
      query: graphQLFunctions.queries.getBudgetForGroup,
      variables: {
        group: this.currentUserGroup.name
      }
    });

    this.groupBudgetQuery.subscribe(({ data }) => {
      this.loading = data.loading;
      this.currentGroupBudget = data.groupBudget.budgets;
      this.cd.markForCheck();
    });

  }

  public initializeGroupCategorySummaryQuery() {
    this.groupCategorySummaryQuery = this.apollo.watchQuery<any>({
      query: graphQLFunctions.queries.getGroupCategorySummary,
      variables: {
        group: this.currentUserGroup.name,
        from: new Date(0),
        to: new Date()
      }
    });

    this.groupCategorySummaryQuery.subscribe(({ data }) => {
      this.currentGroupCategorySummary = data.groupCategorySummary;
      console.log(data.groupCategorySummary);
      const expenseArray = data.groupCategorySummary.map(a => a.value);
      this.currentTotalExpenses = expenseArray.reduce((a, b) => a + b, 0);
      this.cd.markForCheck();
    });

  }

  public initializeGroupAndDateCategorySummaryQuery() {
    this.groupAndDateBudgetQuery = this.apollo.watchQuery<any>({
      query: graphQLFunctions.queries.getGroupCategorySummary,
      variables: {
        group: this.currentUserGroup.name,
        from: new Date(this.currentYear, this.currentMonth, 1, 0, 0),
        to: new Date(this.currentYear, this.currentMonth + 1, new Date(this.currentYear, this.currentMonth + 1, 1).getDate())
      }
    });

    this.groupAndDateBudgetQuery.subscribe(({ data }) => {
      this.currentGroupAndMonthCategorySummary = data.groupCategorySummary;
      this.cd.markForCheck();
    });

  }


  ngAfterViewInit() {
    this.windowWidth = window.innerWidth;
    this.payer = JSON.parse(localStorage.getItem('currentUser')).username;

  }

  @HostListener('window:resize', ['$event'])
  resize(event) {
    this.windowWidth = window.innerWidth;
    this.setDisplayedColumns();
  }

  currentGroupChanged(event) {
    this.setCurrentUserGroup(event.value);
    this.location.replaceState('/budget/' + event.value);
    this.initializeBillsQuery();
    this.initializeBudgetQuery();
    this.initializeGroupBudgetQuery();
    this.initializeGroupCategorySummaryQuery();
    this.initializeGroupAndDateCategorySummaryQuery();
    this.consumerCheckboxes = [];
    for (let i; i < this.currentUserGroup.members.length; i++) {
      this.consumerCheckboxes.push(true);
    }
  }

  setCurrentUserGroup(groupname) {
    const tempUserGroup = this.userGroups.filter((obj) => {
      return (obj.name === groupname);
    });
    if (tempUserGroup.length === 0) {
      this.router.navigate(['/budget/']);
      return false;
    } else {
      this.currentUserGroup = tempUserGroup[0];
      this.category = this.currentUserGroup.categories[0];
      for (let i; i < this.currentUserGroup.members.length; i++) {
        this.consumerCheckboxes.push(true);
      }
      return true;
    }
  }

  // ------------GQL----------------//
  public addBill(sum, name, paidBy, items, billConsumers) {
    let consumers = [];
    if (!billConsumers) {
    for (const i in this.consumerCheckboxes) {
      if (this.consumerCheckboxes[i] === true) {
        consumers.push(this.currentUserGroup.members[i].username);
      }
    }
  } else {
    consumers = billConsumers;
  }

    if (consumers.length === 0) {
      alert('Add at least one consumer!');
    } else {

      this.apollo.mutate({
        mutation: graphQLFunctions.mutations.newBill,
        variables: {
          'input': {
            'items': items,
            'name': name,
            'paidBy': paidBy,
            'date': new Date(),
            'group': this.currentUserGroup.name
          }
        },
      })
        .toPromise()
        .then(({ data }: ApolloQueryResult<any>) => {
          this.zweck = '';
          this.cost = 0;
          this.nameInputField.nativeElement.focus();
          this.snackBar.open('Added new Bill: ' + data.newBill.name, 'Sum: €' + data.newBill.sum, {
            duration: 2000
          });
          if (data.ApolloError) {
            console.log(data.ApolloError.message);
          }
          this.budgetQuery.refetch();
          this.billsQuery.refetch();
          this.groupBudgetQuery.refetch();
          this.groupCategorySummaryQuery.refetch();
          this.cd.markForCheck();

        })
        .catch((errors: any) => {
          console.log('there was an error sending the query', errors);
        });
    }
  }


  public deleteBill(id) {
    const dialogRef = this.dialog.open(ConfirmdeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.apollo.mutate({
          mutation: graphQLFunctions.mutations.deleteBill,
          variables: {
            'id': id
          }
        })
          .toPromise()
          .then(({ data }: ApolloQueryResult<any>) => {
            if (data.ApolloError) {
              console.log(data.ApolloError.message);
            }
            this.snackBar.open('Deleted Bill: ' + data.deleteBill.name, 'Sum: €' + data.deleteBill.sum, {
              duration: 2000
            });
            this.budgetQuery.refetch();
            this.billsQuery.refetch();
            this.groupBudgetQuery.refetch();
            this.groupCategorySummaryQuery.refetch();
            this.cd.markForCheck();
          })
          .catch((errors: any) => {
            console.log('there was an error sending the query', errors);
          });
      }
    });
  }

  // ----------Month Selector-------------------
  // -------------------------------------------
  onSelectChange = ($event: any): void => {
    this.currentMonth = $event.index;
    this.initializeBillsQuery();
    this.initializeGroupAndDateCategorySummaryQuery();
  }

  public selectTab(index) {
    this.currentMonth = index;
    this.initializeBillsQuery();
    this.initializeGroupAndDateCategorySummaryQuery();
  }

  public selectNextMonth() {
    this.currentMonth += 1;
    if (this.currentMonth >= 12) {
      this.currentMonth = 0;
      this.currentYear++;
    }
    this.initializeBillsQuery();
    this.initializeGroupAndDateCategorySummaryQuery();
  }

  public selectPreviousMonth() {
    this.currentMonth -= 1;
    if (this.currentMonth < 0) {
      this.currentMonth = 11;
      this.currentYear--;
    }
    this.initializeBillsQuery();
    this.initializeGroupAndDateCategorySummaryQuery();
  }

  public selectNextYear() {
    this.currentYear++;
    this.initializeBillsQuery();
    this.initializeGroupAndDateCategorySummaryQuery();
  }
  public selectPreviousYear() {
    this.currentYear--;
    this.initializeBillsQuery();
    this.initializeGroupAndDateCategorySummaryQuery();
  }


  // -----DIALOGS--------------
  openAddBillDialog() {
    const dialogRef = this.dialog.open(AddBillDialogComponent,
      {
        'data': {
          'userGroup': this.currentUserGroup,
          'user': this.currentUsername
        },
        height: '80%',
        width: '80%'
      });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addBill(result.sum, result.name, result.payer, result.items, result.billConsumers);
      }
    });
  }

  quickAddBill(sum, name, payer, category) {
    const consumers = [];
    for (const i in this.consumerCheckboxes) {
      if (this.consumerCheckboxes[i] === true) {
        consumers.push(this.currentUserGroup.members[i].username);
      }
    }
    const item = {
      'name': name,
      'consumers': consumers,
      'amount': 1,
      'type': {
        'name': 'n/a',
        'pricePerUnit': 1,
        'unit': 'n/a'
      },
      'price': sum,
      'unitSize': 1,
      'date': new Date(),
      'category': category
    };
    this.addBill(sum, name, payer, [item], null);
  }

  changeExpandedElement(row) {
    if (this.expandedElement === row) {
      this.expandedElement = null;
    } else {
      this.expandedElement = row;
    }
  }

}

class BillsDataSource extends DataSource<any> {
  constructor(private data: any[]) {
    super();
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<any[]> {
    const rows = [];
    this.data.forEach(element => rows.push(element, {detailRow: true, element}));
    return Observable.of(rows);
  }

  disconnect() { }
}

