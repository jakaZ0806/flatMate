import gql from "graphql-tag";


/*---------------QUERIES--------------------*/
const getUserByUsername:any = gql`
        query getUser($username: String) {
          user(username: $username) {
            username
            firstName
            lastName,
            email
            groups {
              name
            }
            admin
          }
        }
      `;

const getUserGroupsByUsername:any = gql`
        query getUser($username: String) {
          user(username: $username) {
            groups {
              name
              description
              members {
                username
                firstName
                lastName
                }
                categories
              }
          }
        }
      `;

const getBudgetForUserAndGroup: any = gql`
    query getBudget($username: String!, $groupname: String!) {
      budget(username: $username, groupname: $groupname)
    }
`;

const getBillsForGroupAndDate: any = gql`
    query billsByGroup($groupname: String!, $from: String, $to: String) {
      billsByGroup(groupname: $groupname, from: $from, to: $to) {
        sum
        date
        name
        id
        paidBy {
          username
          firstName
          lastName
         }
        items {
            name
            price
            consumers {
              firstName
            }
            category
          }

       }
    }

`;

const getBudgetForGroup: any = gql`query getGroupBudget($group: String!) {
        groupBudget(group: $group) {
          group {
            name
          }
          budgets {
            user {
              username
              firstName
              lastName
            }
            budget
          }
        }
      }
`;

const getGroupCategorySummary: any = gql`
        query groupCategorySummary($group:String!, $from: String, $to: String) {
              groupCategorySummary(group: $group, from: $from, to: $to) {
            name
            value
            }
         }
`;


const queries = {getUserByUsername, getBudgetForUserAndGroup, getUserGroupsByUsername, getBillsForGroupAndDate, getBudgetForGroup, getGroupCategorySummary};


/*---------------------MUTATIONS------------------*/
const newBill: any = gql`
    mutation newBill($input:BillInput!) {newBill(input: $input) {
        name
        sum
	      }
    }
`;

const deleteBill: any = gql `
    mutation deleteBill($id: String!) {
          deleteBill(id: $id) {
              sum
              name
            }
        }
`;

const mutations = {newBill, deleteBill};



export {queries, mutations};
