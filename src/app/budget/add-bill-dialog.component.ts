import { Component, OnInit, Inject, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { KeysPipe } from '../shared/keyspipe.pipe';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-add-bill-dialog',
  templateUrl: 'add-bill-dialog.component.html',
  styleUrls: ['add-bill-dialog.component.css'],
  providers: [KeysPipe],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', visibility: 'hidden'})),
      state('expanded', style({height: '*', visibility: 'visible'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])
  ]
})



export class AddBillDialogComponent implements OnInit {

  @ViewChild('') selectionList;

  consumerCheckboxes = [];
  payer;
  restItem = null;
  sum;
  displayedColumns = ['name', 'price'];
  itemsDataSource;
  expandedElement;

  items: any[] = [];


  constructor(public dialogRef: MatDialogRef<AddBillDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public cd:ChangeDetectorRef) { }

  ngOnInit() {
    this.payer = this.data.user;
    for (const i in this.data.userGroup.members) {
      this.consumerCheckboxes.push(true);
    }
    this.createRestItem("Rest");
    this.itemsDataSource = new ItemsDataSource(this.items);
  }

  isExpansionDetailRow = (row: any) => row.hasOwnProperty('detailRow');
  restItemExists = (row: any) => true;

  addItem() {
    let checkboxes = [];
    for (const i in this.data.userGroup.members) {
      checkboxes.push(true);
    }
    const item = {
      "name": "",
      "isRestItem": false,
      "price": 0,
      "consumerCheckboxes": checkboxes,
      "category": this.data.userGroup.categories[0],
      "index": this.items.length
    };

    this.items.splice(this.items.length - 1, 0, item);

    this.updateRestItemPrice();
    this.itemsDataSource = new ItemsDataSource(this.items);
    this.cd.markForCheck();

  }

  removeItem(index) {
      // Since the details-row is an additional index, the actual item to delete is in every other row
      this.items.splice((index - 1) / 2, 1);
      this.updateRestItemPrice();
      this.itemsDataSource = new ItemsDataSource(this.items);
      this.cd.markForCheck();
  }



  createRestItem(name) {
    let checkboxes = [];
    for (const i in this.data.userGroup.members) {
      checkboxes.push(true);
    }
    let itemSum = 0;
    for (const item in this.items) {
      itemSum += this.items[item].price;
    }
    this.items.push({
      "name": name,
      "price": 0,
      "isRestItem": true,
      "consumerCheckboxes": checkboxes,
      "category": this.data.userGroup.categories[0],
      "index": 0
    });

  };

  updateRestItemPrice() {
    let itemSum = 0;
    for (let i = 0; i < this.items.length - 1; i++) {
      itemSum += this.items[i].price;
    }
    this.items[this.items.length - 1].price = Math.round((this.sum - itemSum) * 100) / 100;
  }

  calculateField($event) {
    console.log($event);
  }

  closeDialogAbort() {
    this.dialogRef.close();
  }

  closeDialogSuccess(sum, name, payer) {
    //Create proper Item-Object for GraphQL Mutation
    let resultItems = [];
    let billConsumers = new Set();
    for (let item in this.items) {
      let consumers = [];
      for (const i in this.items[item].consumerCheckboxes) {
        if (this.items[item].consumerCheckboxes[i] === true) {
          consumers.push(this.data.userGroup.members[i].username);
          billConsumers.add(this.data.userGroup.members[i].username);
        }
      }
      resultItems.push(
        {
          "name": this.items[item].name,
          "consumers": consumers,
          "amount": 1,
          "type": {
            "name": "n/a",
            "pricePerUnit": 1,
            "unit": "n/a"
          },
          "price": this.items[item].price,
          "unitSize": 1,
          "date": new Date(),
          "category": this.items[item].category
        }
      );

    }
    this.dialogRef.close({"sum": sum, "name": name, "payer": payer, items: resultItems, billConsumers: billConsumers});
  }

  changeExpandedElement(row) {
      this.expandedElement = row;
    }
}

class ItemsDataSource extends DataSource<any> {
  constructor(private data: any[]) {
    super();
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<any[]> {
    const rows = [];
    this.data.forEach(element => rows.push(element, {detailRow: true, element}));
    return Observable.of(rows);
  }

  disconnect() {}
}
